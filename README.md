# Générateur de Roster d'équipe de Roller Derby
Marre de faire mouliner votre éditeur d'images pendant de fastidieuses minutes (ou heures) à modifier une par une les 20 cases de votre roster pour y ajouter derby names, numéros et photos de vos joueur·euses ?
Cette app est pour vous.

## C'est quoi ?
C'est une petite application web qui permet de générer une image de roster, préférablement au fromat 1920*1080, pour les besoins de communication de votre club.

## Comment ça fonctionne ?
Pour faire fonctionner l'app, il vous faudra :
- Le nom de votre équipe.
- L'**URL du logo** de votre équipe, **préalablement mis en ligne** par le moyen de votre choix.
- La liste de vos jour·euses et Staff comprenant leur numéro (ou position), leur derby name, et l'URL de leur photo **préalablement mise en ligne**, en format **CSV**. ex :
```
005,DerbyName,http://photoderoster.com/photo
1312,name,http://photoderoster.com/photo2
Staff,pseudo,http://photoderoster.com/photo3
```

### Étapes
- Ouvrez le fichier `index.html` dans votre navigateur favori (l'app est optimisée pour Firefox cependant).
- Remplissez le formulaire avec les informations nécessaires recueillies plus tôt.
- Plus qu'à cliquer sur le bouton pour valider le formulaire, et paf !
- Faites f11 pour passer le visuel en plein écran
- Faites-en une capture d'écran.

Et voilà 🎉🎉🎉

# Personnalisation

Vous voudrez certainement personnaliser le roster pour qu'il ressemble un peu plus à l'identité de votre club. Pour ça, je fais en sorte que vous ayez à toucher le moins de code possible !

### Personnaliser la police d'écriture

1. Mettez votre police d'écriture au format woff et woff2 dans le dossier `/assets`
2. Ouvrez le fichier `customfont.css` et mettez à jour le nom du fichier appelé (remplacez `assets/fugazone-regular-webfont.woff2` par `assets/votrefont.woff2` et `assets/fugazone-regular-webfont.woff` par `assets/votrefont.woff`)
3. Ne touchez à rien d'autre, normalement, c'est bon !

### Personnaliser l'image de fond

1. Prenez votre image de fond, idéalement au format 1920 * 1080 puisqu'il s'agit du format pour lequel ce générateur est optimisé, et **au format PNG**.
2. Renommez le fichier `texture.png`.
3. Mettez l'image dans le dossier `/assets` pour remplacer le fichier original.

### Disclaimer 

Pour le moment c'est cassé si on met plus de 23 entrées (genre s'il y a 20 porsonnes au roster et + de 3 en staff, ça passe pas). 
Je vais voir pour adapter la mise en forme pour pouvoir aller jusqu'à 25 !