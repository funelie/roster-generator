var theme = document.querySelector('input[name="theme"]:checked').value;
var team = document.getElementById('team').value;
var logo = logo = document.getElementById('logo').value;
var str = str = document.getElementById('rostercsv').value;

function valTheme() {theme = document.querySelector('input[name="theme"]:checked').value;}
function valName () {team = document.getElementById('team').value;}
function valLogo () {logo = document.getElementById('logo').value;}
function valRoster () {str = document.getElementById('rostercsv').value;}

document.getElementById('rostercsv').addEventListener("change", parseCSV, false);


function addLogo() {
    const logourl = 'url(' + logo + ')';
    document.getElementById("logoimg").style.backgroundImage = logourl;
}

function parseCSV(str) {
    var arr = [];
    var quote = false; 
    for (var row = 0, col = 0, c = 0; c < str.length; c++) {
        var cc = str[c], nc = str[c+1];        // Current character, next character
        arr[row] = arr[row] || [];             // Create a new row if necessary
        arr[row][col] = arr[row][col] || '';   // Create a new column (start with empty string) if necessary
        if (cc == '"' && quote && nc == '"') { arr[row][col] += cc; ++c; continue; }
        if (cc == '"') { quote = !quote; continue; }
        if (cc == ',' && !quote) { ++col; continue; }
        if (cc == '\r' && nc == '\n' && !quote) { ++row; col = 0; ++c; continue; }
        if (cc == '\n' && !quote) { ++row; col = 0; continue; }
        if (cc == '\r' && !quote) { ++row; col = 0; continue; }

        arr[row][col] += cc;
    }
    return arr;
}

function addSkaters() {
    for (let row of parseCSV(str)) {
        var grid = document.getElementById("grid");
        var pic = document.createElement('div');
        var block = document.createElement('div');
        var skaterNumber = document.createElement('h3');
        var skaterName = document.createElement('h4');
        block.classList.add('skater');
        skaterName.classList.add('themed');
        grid.appendChild(block);
        block.appendChild(pic);
        block.appendChild(skaterNumber);
        block.appendChild(skaterName);
        skaterNumber.innerHTML = row[0];
        skaterName.innerHTML = row[1];
        const skaterpic = 'url(' + row[2] + ')';
        pic.style.backgroundImage = skaterpic;
    }
}

function setBackground() {
    var themed = document.getElementsByClassName('themed');
    var themedBg = document.getElementsByClassName('themedbg');
    var skater = document.getElementsByClassName('skater');
    var staff = document.getElementsByClassName('staff');
    var i = 0;
    if (theme == "light") {
        document.getElementById("roster").style.backgroundColor = "#F4F4F4";
        for (i = 0; i < themed.length; i++) {
            themed[i].style.color = "#181818";
        }
        for (i = 0; i < themedBg.length; i++) {
            themedBg[i].style.color = "#F4F4F4";
        }
        for (i = 0; i < skater.length; i++) {
            skater[i].style.backgroundColor = "#F4F4F4";
        }
        for (i = 0; i < staff.length; i++) {
            staff[i].style.backgroundColor = "#F4F4F4";
        }
    } else if (theme == "dark") {
        document.getElementById("roster").style.backgroundColor = "#323232";
        for (i = 0; i < themed.length; i++) {
            themed[i].style.color = "#F4F4F4";
        }
        for (i = 0; i < themedBg.length; i++) {
            themedBg[i].style.color = "#181818";
        }
        for (i = 0; i < skater.length; i++) {
            skater[i].style.backgroundColor = "#181818";
        }
        for (i = 0; i < staff.length; i++) {
            staff[i].style.backgroundColor = "#181818";
        }
    }
} 

function makeRoster() {
    document.getElementById("form").style.display = "none";
    document.getElementById("roster").style.display = "flex";
    document.getElementById("teamname").innerHTML = team;
    addLogo();
    addSkaters();
    setBackground();
}